/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

 // Impliment requestAnimationFrame and cancelAnimationFrame,
//  If the browser/container doesn't already support it.
(function() {
    // debug("Ensuring window.requestAnimationFrame() exhists.")
    var lastTime = 0;
    var startTime = new Date().getTime();
    var vendors = ['ms', 'moz', 'webkit', 'o'];
    for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
        window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
        window.cancelAnimationFrame = window[vendors[x]+'CancelAnimationFrame'] || window[vendors[x]+'CancelRequestAnimationFrame'];
    }

    if (!window.requestAnimationFrame)
        // debug("WARNING: window.requestAnimationFrame() still doesn't exhist, creating fallback using window.setTimeout()");
        window.requestAnimationFrame = function(callback, element) {
            var currTime = new Date().getTime();
            var timeToCall = Math.max(0, 16 - (currTime - lastTime));
            var id = window.setTimeout(function() { callback(currTime - startTime); }, 
              timeToCall);
            lastTime = currTime + timeToCall;
            return id;
        };

    if (!window.cancelAnimationFrame)
        window.cancelAnimationFrame = function(id) {
            clearTimeout(id);
        };
}());

var console = {
    log: function(what) {
        document.getElementById("log").innerHTML += what + "<br>";
    }
};

var app = {
	levelData: [],
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
        document.addEventListener('mousedown', this.interaction.mouse, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        // app.receivedEvent('deviceready');
        var tmp = setTimeout(app.begin, 3000);
    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
    },
    // Everything is loaded, so get ready to enter the main menu
    begin: function() {
        document.getElementById("logo").setAttribute('style', 'display:none;');
        document.getElementById("menu").setAttribute('style', 'display:block;');
        document.getElementById("about").setAttribute('style', 'display:none;');
        document.getElementById("instructions").setAttribute('style', 'display:none;');        
    },
	// Show Main Menu
	showMenu: function() {
		app.playSound();
        document.getElementById("logo").setAttribute('style', 'display:none;');
        document.getElementById("menu").setAttribute('style', 'display:block;');
        document.getElementById("about").setAttribute('style', 'display:none;');
        document.getElementById("instructions").setAttribute('style', 'display:none;');
        document.getElementById("main").setAttribute('style', 'display:none;');
	},
    // Show About Menu
    showAbout: function() {
		app.playSound();
        document.getElementById("logo").setAttribute('style', 'display:none;');
        document.getElementById("menu").setAttribute('style', 'display:none;');
        document.getElementById("about").setAttribute('style', 'display:block;');
        document.getElementById("instructions").setAttribute('style', 'display:none;');
        document.getElementById("main").setAttribute('style', 'display:none;');
    },
    // Show Instructions Menu
    showInstructions: function() {
		app.playSound();
        document.getElementById("logo").setAttribute('style', 'display:none;');
        document.getElementById("menu").setAttribute('style', 'display:none;');
        document.getElementById("about").setAttribute('style', 'display:none;');
        document.getElementById("instructions").setAttribute('style', 'display:block;');
        document.getElementById("main").setAttribute('style', 'display:none;');
    },
    // Play an MP3 sound.
    playSound: function(which) {
        which = which || "flip";
        var tmpFileName = "/android_asset/www/sounds/" + which + ".mp3";
        // var theSound = new Media(tmpFileName,
        //         function () {theSound.release();},
        //         function (err) {console.log("playSound(): " + tmpFileName + " - Error: " + err);}
        //     );
        var theSound = new Media(tmpFileName,
                function () {},
                function (err) {}
            );
        theSound.play();
    },
    // Set up the gameplay variables, generate the play field, enter mainLoop
    startGame: function() {
        document.getElementById("logo").setAttribute("style", "display:none;");
        document.getElementById("menu").setAttribute("style", "display:none;");
        document.getElementById("about").setAttribute("style", "display:none;");
        document.getElementById("instructions").setAttribute("style", "display:none;");
        document.getElementById("main").setAttribute("style", "display:block;");  
        
        var tmpRes = app.getResolution();
        app.width = tmpRes[0];
        app.height = tmpRes[1];
        app.centerX = app.width / 2;
        app.centerY = app.height / 2;
        app.buttonRadius = 45;
        app.rings = [];
        app.score = 100;
        app.canScore = 0;
        app.lastTimeStamp = 0;
        app.desiredFPS = 50;
        app.currentLevel = 1;
        app.halt = false;
        

        //
        app.generateBoard();

        // Set up level 1
        app.setScore(app.score);
        app.drawButton();

        // Begin game
        window.requestAnimationFrame(app.mainLoop);
    },
    // Create the canvases for gameplay
    generateBoard: function() {
        var tmp = "";
        tmp += "<canvas id='frontBuffer' width='" + app.width + "'";
        tmp += " height='" + app.height + "'></canvas>";
        tmp += "<canvas id='backBuffer' width='" + app.width + "'";
        tmp += " height='" + app.height + "' style='display:none;'></canvas>";
        document.getElementById("main").innerHTML += tmp;

        app.frontBuffer = document.getElementById("frontBuffer");
        app.backBuffer = document.getElementById("backBuffer");

        app.frontCtx = app.frontBuffer.getContext("2d");
        app.backCtx = app.backBuffer.getContext("2d");

        app.frontCtx.globalCompositeOperation = 'copy';
    },
    // Generate a random integer with specificed min and max values
    getRandomInt: function(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    },
    // Main game loop
    mainLoop: function(timeStamp) {
        if (app.halt == true) {return;};
        app.startTime = app.startTime || timeStamp;
        app.lastTimeStamp = app.lastTimeStamp || timeStamp;
        delta = timeStamp - app.lastTimeStamp;
        app.seconds = Math.round((app.lastTimeStamp - app.startTime) / 1000);

        if (app.levelData[app.currentLevel].rings[app.seconds] && app.levelData[app.currentLevel].rings[app.seconds].done != true)
        {
            app.levelData[app.currentLevel].rings[app.seconds].done = true;
            window.eval(app.levelData[app.currentLevel].rings[app.seconds].data);
        }
        
        // Update scoreboard
        app.setScore(app.score);
        if (app.score <= 0)
        {
            app.endGame();
            return;
        }
        // Clear screen
        //app.frontCtx.clearRect(0,0, app.width, app.height)
        app.backCtx.clearRect(0,0, app.width, app.height)

        // Draw Buttons
        app.drawButton();

        // Draw Rings
        if (app.rings.length > 0)
        {
            for (var a=0;a<=app.rings.length - 1;a++)
            {
                var tmpShrink = delta / (app.rings[a].speed * app.desiredFPS)  * 1e3;
                if (app.rings[a].remove != 1)
                {
                    app.rings[a].radius = app.rings[a].radius - tmpShrink / 4;
                    if (app.rings[a].radius <= app.buttonRadius)
                    {
                        app.rings[a].waitTime -= 1;
                        if (app.rings[a].canScore == 0)
                        {
                            app.rings[a].canScore = 1;
                            app.canScore += 1;
                        }
                        if (app.rings[a].radius <= 10){app.rings[a].radius = 10;}
                        if (app.rings[a].waitTime <= 0)
                        {
                            if (app.canScore > 0)
                            {
                                app.score -= (10 * app.currentLevel);
                                app.canScore -= 1;
                            }
                            app.rings[a].remove = 1;
                        }
                    }
                app.drawCircle(app.rings[a].radius, app.rings[a].width, app.rings[a].color)
                }
            };
        };

        // Check user input
        
        // 
        app.frontCtx.drawImage(app.backBuffer,0,0);
        app.lastTimeStamp = timeStamp;
        window.requestAnimationFrame(app.mainLoop);
    },
    // Input handler functions
    interaction: (function () {
        return {
            mouse: function(what) {
                //what.preventDefault();
                if(what.x > app.centerX - app.buttonRadius && what.x < app.centerX + app.buttonRadius && what.y > app.centerY - app.buttonRadius && what.y < app.centerY + app.buttonRadius)
                {
                    //console.log(what);
                    //console.log("Valid click detected");
                    if (app.canScore > 0)
                    {
                        app.canScore -= 1;
                        app.score += (20 * app.currentLevel);
                    } else {
                        app.score -= app.currentLevel;
                    }
                }
                //console.log(what);
            },
            touch: function(what) {
                //what.preventDefault();
                console.log(what);
            }
        }
    }()),
    // Determine the current max usable resolution
    getResolution: function() {
        var w = [];
        w[0] = screen.availWidth || 999999999;
        w[1] = window.innerWidth || 999999999;
        w[2] = document.body.clientWidth || 999999999;
        w.sort(function(a, b) {
            return a - b;
        });

        var h = [];
        h[0] = screen.availHeight || 999999999;
        h[1] = window.innerHeight || 999999999;
        h[2] = document.body.clientHeight || 999999999;
        h.sort(function(a, b) {
            return a - b;
        });
        
        return [w[0],h[0]];
    },
    // Draw a circle on the backbuffer
    drawCircle: function(radius, width, color, x, y) {
        var x = x || app.centerX;
        var y = y || app.centerY;
        var radius = radius || 45;
        var width = width || 5;
        var color = color || "#F0F";

        app.backCtx.beginPath();
        app.backCtx.arc(x, y, radius, 0, 2 * Math.PI, false);
        
        app.backCtx.lineWidth = width;
        app.backCtx.strokeStyle = color;
        app.backCtx.shadowColor = color;
        app.backCtx.shadowBlur = 20;
        app.backCtx.shadowOffsetX = 0;
        app.backCtx.shadowOffsetY = 0;
        app.backCtx.closePath();
        app.backCtx.stroke();

        app.backCtx.shadowColor = 0;
        app.backCtx.shadowBlur = 0;
        app.backCtx.shadowOffsetX = 0;
        app.backCtx.shadowOffsetY = 0;
    },
    // Draw a button on the backbuffer
    drawButton: function(x, y, radius) {
        var x = x || app.centerX;
        var y = y || app.centerY;
        var radius = radius || app.buttonRadius;

        var grd = app.backCtx.createRadialGradient(x, y, 1, x, y, radius);
        grd.addColorStop(0, "#222222");
        grd.addColorStop(.6, "#222222");
        grd.addColorStop(.7, "gold");
        grd.addColorStop(.8, "yellow");
        grd.addColorStop(.9, "gold");
        grd.addColorStop(1, "#222222");
        app.backCtx.fillStyle = grd;

        app.backCtx.shadowColor = 0;
        app.backCtx.shadowBlur = 0;
        app.backCtx.shadowOffsetX = 0;
        app.backCtx.shadowOffsetY = 0;

        app.backCtx.fillRect(x - radius - 5, y - radius - 5, radius * 2 + 10, radius * 2 + 10);
    },
    // Draw text on the backbuffer
    drawText: function(what, size) {
        var gameOverContainer = document.getElementById("gameOver");
        var gameOverText = document.getElementById("gameOverText");
        
        var tmp = "";
        tmp += "<font size='" + size + "px'>";
        tmp += what;
        tmp += "</font>";

        gameOverText.innerHTML = tmp;
        gameOverContainer.setAttribute("style", "display:block;");
        var tmpLeft = app.centerX - (gameOverContainer.offsetWidth / 2);
        var tmpTop = app.centerY - (gameOverContainer.offsetHeight / 2);
        var der = gameOverContainer.getAttribute("style");
        der += "top:" + tmpTop + "px;left:" + tmpLeft + "px;";
        gameOverContainer.setAttribute("style", der);
    },
    // Add a ring to the ring stack
    addRing: function(rColor, rWidth, rSpeed, rRadius, rWaitTime) {
        app.rings.push({
            color: rColor,
            width: rWidth,
            speed: rSpeed,
            radius: rRadius,
            waitTime: rWaitTime,
            remove: 0,
            ignoreMe: 0,
            canScore: 0
        });
    },
    // Remove a ring from the ring stack
    removeRing: function(which) {
        app.rings.splice(which,1);
    },
    // Add a randomized ring to the ring stack
    randomRing: function(randomizeColor, bias) {
        var randomizeColor = randomizeColor || 0;
        var bias = bias || [0,0,0];
        if (randomizeColor == true)
        {
            var red = app.getRandomInt(64,255) + bias[0];
            if (red < 0) {red = 0;};
            if (red > 255) {red = 255;};
            var green = app.getRandomInt(64,255) + bias[1];
            if (green < 0) {green = 0;};
            if (green > 255) {green = 255;};
            var blue = app.getRandomInt(64,255) + bias[2];
            if (blue < 0) {blue = 0;};
            if (blue > 255) {blue = 255;};
        } else {
            var brightness = app.getRandomInt(128,255);
            var red = brightness;
            var green = brightness;
            var blue = brightness;
        }
        app.addRing("rgb(" + red + "," + green + "," + blue + ")", app.getRandomInt(5,10), app.getRandomInt(5,20), app.width / 2, 20);
    },
    // Set the actual GUI text for the score
    setScore: function(what) {
        var scoreNumbers = document.getElementById("scoreNumbers");
        var scoreBoard = document.getElementById("scoreBoard");
        var tmp1 = what;
        // tmp1 += ":";
        // tmp1 += app.canScore;
        // tmp1 += "<br>";
        // tmp1 += Math.round((app.lastTimeStamp - app.startTime) / 1000);

        scoreNumbers.innerHTML = tmp1;//what + ":" + app.canScore ;

        var tmpLeft = app.centerX - (scoreBoard.offsetWidth / 2);
        var tmpTop = app.centerY - (scoreBoard.offsetHeight / 2);
        scoreBoard.style.left = tmpLeft + "px";
        // var der = scoreBoard.getAttribute("style");
        // der += "top:" + tmpTop + "px;left:" + tmpLeft + "px;";
        // gameOverContainer.setAttribute("style", der);        
    },
    // Do the end of game stuff, like display game over
    gameOver: function(msg) {
        var msg = msg || "Game Over";
        app.drawText(msg, 48);
    },
    // End the currently running game
    endGame: function(msg) {
        var msg = msg || "Game Over";
        app.halt = true;
        app.gameOver(msg);
    }    
};

app.initialize();